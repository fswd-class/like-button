const countspan = document.getElementById('count')
const childBox1 = document.getElementById('childBox1')

let count = 0
childBox1.addEventListener('click', incrementCount)

function incrementCount(){
    count = count + 1
    countspan.innerHTML = count
}
